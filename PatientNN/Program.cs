﻿using System;
using System.Collections.Generic;

namespace PatientNN
{
	class Program
	{
		public static void Main(string[] args)
		{
			Perceptron p = new Perceptron(2);
			var trainingList = new List<Patient>();
			var trainingOutput = new List<Int32>();
			string[] lines = System.IO.File.ReadAllLines("trainingData.txt");
			foreach (string line in lines) {
				string[] inputs = line.Split(' ');
				trainingList.Add(new Patient(Int32.Parse(inputs[0]), Int32.Parse(inputs[1])));
				trainingOutput.Add(Int32.Parse(inputs[2]));
			}
			for (int j = 0; j < 6000; j++) {
				for (int i = 0; i < trainingOutput.Count; i++) {
					p.train(trainingList[i], trainingOutput[i]);
					
				}
			}
			double[] b = { 18, 0 };
			Console.WriteLine(p.guess(b));

			Console.ReadKey(true);
		}
	}
}
