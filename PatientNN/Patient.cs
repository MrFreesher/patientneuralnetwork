﻿
using System;

namespace PatientNN
{

	public class Patient
	{
		public int age {get;set;}
		public int smokeAgeLength {get; set;}
		public Patient(int patientAge,int patientSmokeAgeLength){
			this.age = patientAge;
			this.smokeAgeLength = patientSmokeAgeLength;
					
		}	
	}

}
