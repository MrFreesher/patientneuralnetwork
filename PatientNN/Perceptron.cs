﻿
using System;

namespace PatientNN
{

	public class Perceptron
	{
		double[] weights { get; set; }
		int countOfWeights { get; set; }
		Random r = new Random();
		public Perceptron(int amountOfWeights)
		{
			countOfWeights = amountOfWeights;
			weights = new double[amountOfWeights];
			for (int i = 0; i < countOfWeights; i++) {
				weights[i] = r.NextDouble() * 2 - 1;
			}
		}
		
		double sigmoid(double sum)
		{
			return 1 / (1 + Math.Exp(-sum));
		}
		public double guess(double[] inputs)
		{
			double sum = 0;
			for (int i = 0; i < countOfWeights; i++) {
				sum = inputs[i] * weights[i];
			}
			
			return  sigmoid(sum);
		}
		
		public void train(Patient p, int target)
		{
			double[] input = { p.age, p.smokeAgeLength };
			double guessValue = guess(input);
			double error = target - guessValue;
			for (int i = 0; i < countOfWeights; i++) {
				
				weights[i] = error * input[i];
			}
			
		}
	}
}
